Maritime Interpolation
================

# Observed path of a vessel

![alt text](observed_path.png)

# Inferred path of the vessel

![alt text](interpolated_path.png)
